--View for Sales Revenue by Category (Current Quarter)--

CREATE VIEW sales_revenue_by_category_qtr AS
SELECT fc.category_id, c.name AS category, SUM(p.amount) AS total_revenue
FROM film_category fc
JOIN category c ON fc.category_id = c.category_id
JOIN inventory i ON fc.film_id = i.film_id
JOIN rental r ON i.inventory_id = r.inventory_id
JOIN payment p ON r.rental_id = p.rental_id
WHERE EXTRACT(QUARTER FROM r.rental_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
GROUP BY fc.category_id, c.name
HAVING SUM(p.amount) > 0;


--Function to Get Sales Revenue by Category (Current Quarter)--

CREATE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INT)
RETURNS TABLE (category_id INT, category VARCHAR(255), total_revenue DECIMAL(5,2))
IS DETERMINISTIC
BEGIN
  RETURN (
    SELECT fc.category_id, c.name AS category, SUM(p.amount) AS total_revenue
    FROM film_category fc
    JOIN category c ON fc.category_id = c.category_id
    JOIN inventory i ON fc.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    JOIN payment p ON r.rental_id = p.rental_id
    WHERE EXTRACT(QUARTER FROM r.rental_date) = current_quarter
    GROUP BY fc.category_id, c.name
    HAVING SUM(p.amount) > 0
  );
END;


--Procedure to Add New Movie--

DROP PROCEDURE IF EXISTS new_movie;

CREATE PROCEDURE new_movie(IN movie_title VARCHAR(255))
BEGIN
  DECLARE new_film_id INT;
  DECLARE lang_exists BOOLEAN DEFAULT FALSE;

  -- Verify language exists
  SELECT EXISTS(SELECT * FROM language WHERE name = 'Klingon') INTO lang_exists;

  IF NOT lang_exists THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Language does not exist';
  END IF;

  -- Generate unique film ID
  START TRANSACTION;
  SELECT MAX(film_id) + 1 INTO new_film_id FROM film;
  INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
  VALUES (new_film_id, movie_title, 4.99, 3, 19.99, YEAR(CURRENT_DATE), (SELECT language_id FROM language WHERE name = 'Klingon'));
  COMMIT;
END;
